import React from 'react'
import { Link } from "react-router-dom"

import Routing from './../../services/Routing'

export default () => (
  <header>
    <div className="flex">
      <ul>
        <li>
          <Link to={Routing.home.path}>
            Home
          </Link>
        </li>
        <li>
          <Link to={Routing.about.path}>
            About
          </Link>
        </li>
        <li>
          <Link to={Routing.projects.path}>
            Projects
          </Link>
        </li>
        <li>
          <Link to={Routing.contact.path}>
            Contact
          </Link>
        </li>
        <li>
          <Link to={Routing.pages.path}>
            Pages
          </Link>
        </li>
      </ul>
    </div>
  </header>
)