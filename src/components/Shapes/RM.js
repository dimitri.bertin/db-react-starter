import React from 'react'

export default ({width, height}) => (
  <svg className="shape shape--rm" width={width} height={height} viewBox="0 0 38 15">
    <path d='M33.2,15c0.1-0.5,0.3-1,0.3-1.9V2.2h0L28.4,15h-3l-5-12.8h0v10.9c0,0.9,0.1,1.4,0.3,1.9 H18c0.1-0.5,0.3-1,0.3-1.9V1.9C18.3,1,18.1,0.5,18,0h5.9l4.3,10.9L32.4,0H38c-0.1,0.5-0.3,1-0.3,1.9v11.1c0,0.9,0.1,1.4,0.3,1.9 H33.2z M15,15h-4.8L7.7,8.4H4.5v4.7c0,0.9,0.1,1.4,0.3,1.9H0c0.1-0.5,0.3-1,0.3-1.9V1.9C0.3,1,0.1,0.5,0,0h11.5 c1.9,0,2.8,1.1,2.8,2.8V6c0,1.4-0.7,2.3-2.2,2.5l1.8,4.6C14.1,13.8,14.4,14.4,15,15z M10.5,5.5V3.1c0-0.7-0.2-1.1-0.8-1.1H4.5v4.5 h5.2C10.3,6.5,10.5,6.3,10.5,5.5z'></path>
  </svg>
)