import React from 'react'

export default ({width, height}) => (
  <svg className="shape shape--twitter" width={width} height={height} viewBox="0 0 24 24">
    <path d='M21.1,9.2c0.8-0.5,1.3-1.2,1.6-2c-0.8,0.4-1.6,0.7-2.4,0.9c-0.7-0.8-1.6-1.1-2.7-1.1c-1,0-1.9,0.4-2.6,1.1 c-0.7,0.7-1.1,1.6-1.1,2.6c0,0.3,0,0.6,0.1,0.8C11,11.3,8.4,10,6.4,7.5C6.1,8.1,5.9,8.8,5.9,9.4c0,1.3,0.5,2.3,1.6,3.1 c-0.6-0.1-1.2-0.2-1.6-0.5c0,0.9,0.3,1.7,0.8,2.4c0.5,0.7,1.3,1.1,2.1,1.3c-0.3,0.1-0.6,0.1-1,0.1c-0.3,0-0.5,0-0.7-0.1 c0.2,0.8,0.7,1.4,1.3,1.8c0.6,0.5,1.3,0.7,2.2,0.7c-1.3,1-2.9,1.6-4.6,1.6c-0.4,0-0.7,0-0.9,0c1.7,1.1,3.6,1.6,5.7,1.6 c2.1,0,4-0.5,5.7-1.6c1.6-1.1,2.9-2.4,3.6-4c0.8-1.6,1.2-3.2,1.2-4.9v-0.5c0.8-0.6,1.4-1.2,1.8-1.9C22.5,8.9,21.8,9.1,21.1,9.2z'></path>
  </svg>
)