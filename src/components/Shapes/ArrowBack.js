import React from 'react'

export default ({width, height}) => (
  <svg className="shape shape--arrow-back" width={width} height={height} viewBox="0 0 24 24">
    <path d='M16,8.02038361 C19.2805754,8.28860929 22,11.1676859 22,14.5 C22,18 19,21 15.500765,21 L3,21 L3,19 L15.500765,19 C18,19 20,17 20,14.5 C20,12 18,10 15.500765,10 L6.83,10 L10.41,13.59 L9,15 L3,9 L9,3 L10.42,4.41 L6.83,8 L16,8 L16,8.02038361 Z'></path>
  </svg>
)