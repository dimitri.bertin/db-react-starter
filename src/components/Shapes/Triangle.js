import React from 'react'

export default ({width, height}) => (
  <svg width={width} height={height} viewBox="0 0 12 12">
      <polygon points="6 0 12 12 0 12"></polygon>
  </svg>
)