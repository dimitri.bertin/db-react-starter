import React from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Layout from './Layout/'
import Routing from './../services/Routing'

export default () => (
  <div className="app-router">
    <Router>
      <Layout>
        <Switch>
          {
             Object.keys(Routing).map((route, index) => (
              <Route path={ Routing[route].path } component={ Routing[route].component } exact={Routing[route].exact || false} key={index} />
            ))
          }
        </Switch>
      </Layout>
    </Router>
  </div>
)