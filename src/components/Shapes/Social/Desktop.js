import React from 'react'

export default ({width, height}) => (
  <svg className="shape shape--desktop" width={width} height={height} viewBox="0 0 24 24">
    <path d=''></path>
  </svg>
)