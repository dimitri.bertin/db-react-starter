import React from 'react'
import Header from './Header'
import Footer from './Footer'

export default ({children}) => (
  <div className="app-layout">
    <Header />
    <div className="app-body">
      { children }
    </div>
    <Footer />
  </div>
)