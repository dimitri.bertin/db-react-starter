import  * as Container from './../scenes/Container'

export default {
  home: {
    component: Container.Home,
    path: '/',
    exact: true,
  },
  about: {
    component: Container.About,
    path: '/about',
    exact: true,
  },
  pages: {
    component: Container.Pages,
    path: '/pages',
    exact: true,
  },
  contact: {
    component: Container.Contact,
    path: '/contact',
    exact: true,
  },
  projects: {
    component: Container.Projects,
    path: '/projects',
    exact: true,
  },
  project: {
    component: Container.Project,
    path: '/projects/:slug',
  },
  sitemap: {
    component: Container.Sitemap,
    path: '/sitemap',
    exact: true,
  },
}