import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Field extends Component {
  state = {
    value: '',
    patternEmail: '',
  }

  componentDidMount() {
    const { value } = this.props
    this.changeValue(value !== '' || typeof value !== 'undefined' ? value : this.input.value)
  }

  handleChange = (event) => {
    this.changeValue(event.target.value)
  }

  changeValue = (val) => {
    // if (typeof val === 'undefined') val = this.input.value
    this.setState({value: val})
    this.props.onChange({key: this.props.name, value: val})
  }

  iEmail = (email) => {
    // Test pattern Email
  }

  static defaultProps = {
    big: false,
  }

  render() {
    const { children, id, type, name, className, big} = this.props
    return(
      <div className={ `form-field ${FieldStyle(this.state.value === '', big)}` }>
        <input type={type} id={id} name={name} value={this.state.value || ''} ref={(input) => this.input = input} className={className} onChange={this.handleChange} />
        {
          type !== 'hidden' && (
            <label htmlFor={id}>{ children }</label>
          )
        }
      </div>
    )
  }
}

Field.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  big: PropTypes.bool,
}