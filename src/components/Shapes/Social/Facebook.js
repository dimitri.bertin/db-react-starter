import React from 'react'

export default ({width, height}) => (
  <svg className="shape shape--facebook" width={width} height={height} viewBox="0 0 24 24">
    <path d='M16.6,9.2h2.1v-3h-2.1c-1.1,0-2,0.4-2.8,1.2c-0.7,0.8-1.1,2.3-1.1,3.5v1.3h-3v3h3v7h3v-7h3v-3h-3v-2.1 C15.7,9.6,16.1,9.2,16.6,9.2z'></path>
  </svg>
)